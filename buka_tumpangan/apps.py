from django.apps import AppConfig


class BukaTumpanganConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'buka_tumpangan'

from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseForbidden, JsonResponse
from nebengdongapp.models import *
from nebengdongapp.models.enum_model import *
import uuid
from django.views.decorators.csrf import csrf_exempt

# Create new tumpangan
@login_required(login_url='authentication:login')
def buka_tumpangan(request):
    pembuka_tumpangan = Pengguna.objects.get(user=request.user)
    if request.method == 'POST':
        lokasi_asal = request.POST.get("lokasi_asal")
        lokasi_tujuan = request.POST.get("lokasi_tujuan")
        depart = request.POST.get("depart")
        kapasitas = request.POST.get("kapasitas")
        tarif = request.POST.get("tarif")
        deskripsi = request.POST.get("deskripsi")

        tumpangan = Tumpangan(
                            id= uuid.uuid4(),
                            lokasi_asal=lokasi_asal,
                            lokasi_tujuan=lokasi_tujuan,
                            depart=depart,
                            kapasitas=kapasitas,
                            tarif=tarif,
                            status='OP',
                            deskripsi=deskripsi,
                            pembuka_tumpangan=pembuka_tumpangan
                        )
        tumpangan.save()

        return JsonResponse({
              "message": "Successfully Add New Tumpangan",
              "pk": tumpangan.id, 
                "fields": {
                    "lokasi_asal":lokasi_asal,
                    "lokasi_tujuan":lokasi_tujuan,
                    "depart":depart,
                    "kapasitas":kapasitas,
                    "tarif":tarif,
                    "status":'OP',
                    "deskripsi":deskripsi
                    }
            }, status=200)
        
    context = {
        "lokasi_choices": [{
            'value': choice[1],
            'label': choice[0]
        } for choice in LokasiDiUI.choices],
        'is_on_tumpangan': pembuka_tumpangan.is_on_tumpangan
    }
    return render(request, "buka_tumpangan.html", context)

# Update tumpangan
@login_required(login_url='authentication:login')
def update_pengajuan(request, pengajuan_id, status):
    pengajuan = PengajuanTumpangan.objects.filter(pk=pengajuan_id).first()
    pengajuan.status_pengajuan = status
    pengajuan.save()

    notifikasi = NotifikasiPengajuanTumpangan(
        id=uuid.uuid4(),
        user=pengajuan.pencari_tumpangan,
        pengajuan_tumpangan=pengajuan
    )
    notifikasi.save()

    return JsonResponse(
            {
                "message": f"Berhasil memperbarui status pengajuan {pengajuan_id} menjadi {status}",
            }, 
            status=200)
    
# konfirmasi tumpangan
@csrf_exempt
@login_required(login_url='authentication:login')
def confirm_tumpangan(request, tumpangan_id):
    user = request.user
    tumpangan = Tumpangan.objects.get(pk=tumpangan_id)
    pembuka_tumpangan = tumpangan.pembuka_tumpangan
    
    # jika pembuka tumpangan
    if pembuka_tumpangan.user_id == user.id:
        # update status tumpangan
        tumpangan.status = StatusTumpangan.SELESAI
        pembuka = Pengguna.objects.get(pk=pembuka_tumpangan.id)
        pembuka.is_on_tumpangan = False
        tumpangan.save()
        pembuka.save()
        
    # jika penumpang
    else:
        # get penumpang and pengajuan
        penumpang = Pengguna.objects.get(user_id = user.id)
        pengajuan = PengajuanTumpangan.objects.get(pencari_tumpangan=penumpang, tumpangan=tumpangan)
        
        # update status pengajuan
        pengajuan.status_pengajuan = StatusPengajuanTumpangan.SELESAI
        penumpang.is_on_tumpangan = False
        pengajuan.save()
        penumpang.save()

    return JsonResponse(
            {
                "message": f"Selamat! Anda telah sampai tujuan, terima kasih telah menggunakan nebengDong!"
            }, 
            status=200)



# konfirmasi tumpangan
@login_required(login_url='authentication:login')
def close_offer(request, tumpangan_id):
    tumpangan = Tumpangan.objects.get(pk=tumpangan_id)
    pengguna_id = Pengguna.objects.get(user_id=request.user.id)
    if pengguna_id == tumpangan.pembuka_tumpangan:
        tumpangan.status = StatusTumpangan.OTW
        tumpangan.save()

        pembuka_tumpangan = tumpangan.pembuka_tumpangan
        pembuka_tumpangan.is_on_tumpangan = True
        pembuka_tumpangan.save()

        pengajuan_diterima = []
        pengajuan_ditolak = []
        pengajuan_acc = PengajuanTumpangan.objects.filter(tumpangan_id=tumpangan_id)
        for pengajuan in pengajuan_acc:
            penumpang = Pengguna.objects.get(id=pengajuan.pencari_tumpangan.id)
            if pengajuan.status_pengajuan == 'DITERIMA':
                penumpang.is_on_tumpangan = True
                pengajuan_diterima.append(pengajuan.id)
                penumpang.save()
            elif pengajuan.status_pengajuan == 'MENGAJUKAN' or pengajuan.status_pengajuan == 'Mengajukan':
                pengajuan.status_pengajuan = 'DITOLAK'
                pengajuan.save()
                pengajuan_ditolak.append(pengajuan.id)

        return JsonResponse(
                {
                    "message": "Successfully Close Offer",
                    "pengajuan_ditolak": pengajuan_ditolak,
                    "pengajuan_diterima": pengajuan_diterima
                }, 
                status=200)
    else:
        return JsonResponse({"error": "You do not have permission to access this resource."}, status=403)        

# Delete tumpangan
@login_required(login_url='authentication:login')
def delete_tumpangan(request, tumpangan_id):
    tumpangan = Tumpangan.objects.filter(pk=tumpangan_id).first()
    pengguna_id = Pengguna.objects.get(user_id=request.user.id)
    if pengguna_id == tumpangan.pembuka_tumpangan:
        tumpangan.delete()
        return JsonResponse(
                {
                    "message": "Successfully Delete Tumpangan"
                }, 
                status=200)
    else:
        return JsonResponse({"error": "You do not have permission to access this resource."}, status=403)
    
@login_required(login_url='authentication:login')
def check_no_kendaraan(request):
    pengguna = Pengguna.objects.get(user_id=request.user.id)
    if pengguna.no_wa == '' and pengguna.id_line == '':
        return JsonResponse({
                                "message": "Kamu belum mengisi informasi kontak. Lengkapi dulu yuk"
                            }, status=200)
    elif pengguna.no_kendaraan == '':
        return JsonResponse(
                {
                    "message": "Kamu belum mengisi nomor kendaraan. Lengkapi dulu yuk"
                }, 
                status=200)
    else:
        return JsonResponse({
                                "message": "success"
                            }, status=200)


@login_required(login_url='authentication:login')
def show_detail_tumpangan(request, id):
    tumpangan = Tumpangan.objects.get(id=id)
    pengguna_id = Pengguna.objects.get(user_id=request.user.id)

    if pengguna_id == tumpangan.pembuka_tumpangan:
        context = {
            'id': tumpangan.id,
            'lokasi_asal': tumpangan.lokasi_asal,
            'lokasi_tujuan': tumpangan.lokasi_tujuan,
            'depart': tumpangan.depart,
            'kapasitas': tumpangan.kapasitas,
            'tarif': tumpangan.tarif,
            'deskripsi': tumpangan.deskripsi,
            'status': tumpangan.get_status_tumpangan(),
        }
        return render(request, "manage_tumpangan.html", context)
    else:
        return JsonResponse({"error": "You do not have permission to access this resource."}, status=403)
    
@login_required(login_url='authentication:login')
def get_pengajuan(request, tumpangan_id):
    tumpangan = Tumpangan.objects.get(id=tumpangan_id)
    pengguna_id = Pengguna.objects.get(user_id=request.user.id)

    if pengguna_id == tumpangan.pembuka_tumpangan:
        pengajuan_list = PengajuanTumpangan.objects.filter(tumpangan=tumpangan)
        pengajuan_diterima = pengajuan_list.filter(status_pengajuan='DITERIMA')
        
        data = []
        for pengajuan in pengajuan_list:
            pengguna = Pengguna.objects.get(id=pengajuan.pencari_tumpangan.id)
            data.append({
                "pengajuan": pengajuan.serialize(),
                "pengguna": pengguna.serialize()
            })
        response = {
            'jml_diterima': len(pengajuan_diterima),
            'data': data
        }
        return JsonResponse(response, safe=False)
    else:
        return JsonResponse({"error": "You do not have permission to access this resource."}, status=403)
from django.urls import path
from .views import *

app_name = 'buka_tumpangan'

urlpatterns = [
    path('new/', buka_tumpangan, name='buka_tumpangan'),
    path('detail/<str:id>', show_detail_tumpangan, name='show_detail_tumpangan'),
    path('update/<str:pengajuan_id>/<str:status>/', update_pengajuan, name='update_pengajuan'),
    path('delete/<str:tumpangan_id>', delete_tumpangan, name='delete_tumpangan'),
    path('confirm_tumpangan/<str:tumpangan_id>/', confirm_tumpangan, name='confirm_tumpangan'),
    path('close-offer/<str:tumpangan_id>/', close_offer, name='close_offer'),
    path('get-pengajuan/<str:tumpangan_id>/', get_pengajuan, name='get-pengajuan'),
    path('check-no-kendaraan/', check_no_kendaraan, name='check_no_kendaraan'),
]
import os
from uuid import UUID
from django.shortcuts import redirect, render
from django.utils import timezone
from django.http import JsonResponse
from nebengdongapp.models.laporan_model import *
from nebengdongapp.models.user_model import *
from nebengdongapp.models.tumpangan_model import *
from django.contrib.auth.models import User
import random

base_link = os.getenv('APP_URL')

def check_pelapor(request, id_tumpangan):
    user = request.user
    tumpangan = Tumpangan.objects.get(pk=id_tumpangan)
    pembuka_tumpangan = tumpangan.pembuka_tumpangan
    
    if pembuka_tumpangan.user_id == user.id:
        list_pengaju_tumpangan = PengajuanTumpangan.objects.filter(tumpangan = id_tumpangan)
        if list_pengaju_tumpangan.exists():
            context = {
                'base_link': base_link,
                'is_penumpang_exist': list_pengaju_tumpangan.exists(),
                'list_penumpang': list_pengaju_tumpangan
            }
        else:
            context = {
                'message' : 'Tidak ada penumpang!'
            }
        return render(request, 'list_penumpang.html', context)
    # jika penumpang
    else:
        return redirect(f'{base_link}/report/0/{id_tumpangan}')
    

def check_then_create(request, flag, id):
    if flag == 0:
        return create_laporan_as_penumpang(request, id)
    else:
        return create_laporan_as_pembuka_tumpangan(request, id)

def create_laporan_as_penumpang(request, id_tumpangan):
    tumpangan = Tumpangan.objects.get(pk=id_tumpangan)
    pembuka_tumpangan = tumpangan.pembuka_tumpangan
    terlapor1 = Pengguna.objects.get(pk = pembuka_tumpangan.id)
    terlapor = Terlapor.objects.get(pengguna_terlapor = terlapor1)

    if request.method == 'POST':           
        create_laporan(request, terlapor)
    
    return render_form_page(request, terlapor)

def create_laporan_as_pembuka_tumpangan(request, id_penumpang):
    terlapor1 = Pengguna.objects.get(pk = id_penumpang)
    terlapor = Terlapor.objects.get_or_create(pengguna_terlapor = terlapor1)

    if request.method == 'POST': 
        create_laporan(request, terlapor)
    
    return render_form_page(request, terlapor)
    
def create_laporan(request, terlapor):
    pelapor1 = Pengguna.objects.get(user = request.user.id)
    pelapor = Pelapor.objects.get_or_create(pengguna_pelapor = pelapor1)

    kategori = request.POST.get('kategori_laporan')
    keterangan = request.POST.get('deskripsi')

    laporan = Laporan(
        tim_assigned=select_random_tim_layanan(),
        pelapor=pelapor,
        terlapor=terlapor,
        timestamp=timezone.now(),
        status='LP',
        kategori=kategori,
        keterangan=keterangan
    )

    laporan.save()

    return JsonResponse({
        "message" : "Laporan berhasil dikirim"
    })
    

def select_random_tim_layanan():
    # Memilih Tim Layanan secara random
    tim_count = User.objects.count()
    random_num = random.randint(0, tim_count-1)
    tim_layanan = User.objects.all()[random_num]

    return tim_layanan

def render_form_page(request, terlapor):
    context = {
        'base_link': base_link,
        "kategori_choices": [{
            'value': choice[1],
            'label': choice[0]
        } for choice in KategoriLaporan.choices],
        "terlapor": terlapor
    }
    return render(request, "report_form.html", context)

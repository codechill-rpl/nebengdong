from django.urls import path
from .views import *

app_name = 'report_pengguna'

urlpatterns = [
    path('<uuid:id_tumpangan>/', check_pelapor, name='check_pelapor'),
    path('<int:flag>/<uuid:id>/', check_then_create, name='check_then_create'),
]
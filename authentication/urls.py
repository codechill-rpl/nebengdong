from django.urls import path
from .views import *

app_name = 'authentication'

urlpatterns = [
    path('login/', login_pengguna.as_view(), name='login_pengguna'),
    path('logout/', logout_user.as_view(), name='logout'),
    path('login_tim/', login_tim, name='login_tim'),
    path('home/', home, name='home'),
    path('create_superuser/', create_superuser_view, name='create_superuser'),
]
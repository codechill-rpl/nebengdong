import os
from django.contrib.auth import login, logout, authenticate
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from django.conf import settings

import xmltodict
import urllib3
import json
import uuid

from nebengdongapp.models import Pengguna

# Create your views here.
# Login pengguna using SSO UI
class login_pengguna(APIView):
    def get(self, request):
        try:
            ticket = request.GET.get("ticket")
            http = urllib3.PoolManager()
            
            response = http.request('GET', f"{os.environ.get('SSO_URL')}/serviceValidate?ticket={ticket}&service={os.environ.get('APP_URL')}/api/auth/login/")

            rawdata = response.data.decode('utf-8')
            data = xmltodict.parse(rawdata)
            data = data.get('cas:serviceResponse').get('cas:authenticationSuccess')

            print(data)
            if(not data):
                return Response(
                    {
                        "error": "Invalid Ticket"
                    }, 
                    status=status.HTTP_400_BAD_REQUEST
                )
            
            user, created = User.objects.get_or_create(
                username=data.get('cas:user'), 
                email=f"{data.get('cas:user')}@ui.ac.id"
            )
                            
            if(created):
                username=data.get('cas:user')
                data = data.get("cas:attributes")
                name = data.get('cas:nama')
                
                i = name.rfind(' ')
                first_name, last_name = name[:i], name[i + 1:]
                
                user.first_name = first_name
                user.last_name = last_name
                user.save()
                
                ORG_CODE = {}
                LANG = settings.SSO_UI_ORG_DETAIL_LANG
                
                with open(settings.SSO_UI_ORG_DETAIL_FILE_PATH, 'r') as ORG_CODE_FILE:
                    ORG_CODE.update(json.load(ORG_CODE_FILE))
                    
                organization = ORG_CODE[LANG][data.get("cas:kd_org")];
                
                pengguna, created = Pengguna.objects.get_or_create(
                    id= uuid.uuid4(),
                    user=user,
                    nama=first_name + " " + last_name,
                    email=f"{username}@ui.ac.id",
                    fakultas=organization.get('faculty'),
                    no_wa="",
                    id_line="",
                    no_kendaraan="",
                    is_blocked=False,
                    is_on_tumpangan=False
                )
                            
            pengguna = Pengguna.objects.filter(user=user).first()   
                       
            if pengguna.is_blocked:
                return JsonResponse({
                "status": False,
                "message": "Failed to Login, Account Disabled."
                }, status=401)
            
            login(request, user)
            if created:
                return HttpResponseRedirect("/profile/profile-pengguna/") # to complete profile first
            return HttpResponseRedirect("/")
               
        except urllib3.exceptions.HTTPError as e:
            print(e)
            return Response(
                {
                    "error": e
                }, 
                status=status.HTTP_400_BAD_REQUEST
            )
        except Exception as e:
            print(e)
            return Response(
                {
                    "error": "Operation Failed"
                }, 
                status=status.HTTP_500_INTERNAL_SERVER_ERROR
            )
            
# Logout Pengguna & Tim
class logout_user(APIView):
    def get(self, request):
        try:
            logout(request)
            return HttpResponseRedirect("/")
        except Exception as e:
            print(e)
            return Response(
                {
                    "error": "Operation Failed"
                }, 
                status=status.HTTP_500_INTERNAL_SERVER_ERROR
            )
        
# Login as tim layanan
@csrf_exempt
def login_tim(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                # Redirect to a success page.
                return JsonResponse({
                "status": True,
                "message": "Successfully Logged In!",
                "username": username
                }, status=200)
            else:
                return JsonResponse({
                "status": False,
                "message": "Failed to Login, Account Disabled."
                }, status=401)

        else:
            return JsonResponse({
            "status": False,
            "message": "Failed to Login, check your email/password."
            }, status=401)

def home(request):
    context = {
        'SSO_URL': os.environ.get('SSO_URL'),
        'APP_URL': os.environ.get('APP_URL'),
    }
    return render(request, 'login.html', context)

@csrf_exempt
def create_superuser_view(request):
    if request.method == 'POST':
        try:
            # Parse the JSON data from the request body
            data = json.loads(request.body.decode('utf-8'))
            username = data.get('username')
            password = data.get('password')
            first_name = data.get('first_name')

            # Check if the superuser already exists
            if not User.objects.filter(username=username).exists():
                # Create a superuser
                user = User.objects.create_superuser(
                    username=username,
                    password=password,
                    first_name=first_name
                )
                return JsonResponse({'message': f'Superuser "{username}" created successfully.'})
            else:
                return JsonResponse({'message': f'Superuser "{username}" already exists.'})
        except json.JSONDecodeError:
            return JsonResponse({'error': 'Invalid JSON format in the request body'})
    else:
        return JsonResponse({'error': 'Invalid request method'})
import django_filters
from nebengdongapp.models import *
from nebengdongapp.models.enum_model import *

class TumpanganFilter(django_filters.FilterSet):
    class Meta:
        model = Tumpangan
        fields = {
            'lokasi_asal': ['exact'],
            'lokasi_tujuan': ['exact'],
            'kapasitas': ['gte'],
            'tarif': ['lte']
        }
import uuid
from django.shortcuts import render, redirect
from django.template.defaulttags import register
from django.contrib.auth.decorators import login_required
from cari_tumpangan.filters import TumpanganFilter
from nebengdongapp.models import *
from nebengdongapp.models.enum_model import *

@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)

# Render Cari Tumpangan main page
@login_required(login_url='authentication:login_pengguna')
def all_tumpangan(request):
    pengguna = Pengguna.objects.get(user_id=request.user.id)
    tumpangan_list = Tumpangan.objects.filter(status=StatusTumpangan.BUKA)
    
    tumpangan_filter = TumpanganFilter(request.GET, queryset=tumpangan_list)
    tumpangan_list = tumpangan_filter.qs.exclude(pembuka_tumpangan=pengguna)

    pengajuan_tumpangan_dict = {}
    for tumpangan in tumpangan_list:
        pengajuan_tumpangan_dict[tumpangan.id] = PengajuanTumpangan.objects.filter(tumpangan=tumpangan, pencari_tumpangan=pengguna).exists()
    
    context = {
        'pengguna': pengguna,
        'tumpangan_list': tumpangan_list,
        'tumpangan_filter': tumpangan_filter,
        'pengajuan_tumpangan_dict': pengajuan_tumpangan_dict,
    }

    return render(request, "cari_tumpangan.html", context)

# Render detail page of a Tumpangan instance
@login_required(login_url='authentication:login_pengguna')
def tumpangan_detail(request, tumpangan_id):
    pengguna = Pengguna.objects.get(user_id=request.user.id)
    tumpangan = Tumpangan.objects.get(id=tumpangan_id)
    context = {
        'pengguna': pengguna,
        'tumpangan': tumpangan,
        'booked': PengajuanTumpangan.objects.filter(tumpangan=tumpangan).exists()
    }

    return render(request, "detail_tumpangan.html", context)

# Apply user as passanger candidate in Tumpangan
@login_required(login_url='authentication:login_pengguna')
def apply_tumpangan(request):
    if request.method == 'POST':
        pencari_tumpangan = Pengguna.objects.get(user_id=request.user.id)
        
        tumpangan_id = request.POST.get("tumpangan_id")
        tumpangan = Tumpangan.objects.get(id=tumpangan_id)

        pengajuan = PengajuanTumpangan(
            id= uuid.uuid4(),
            status_pengajuan=StatusPengajuanTumpangan.MENGAJUKAN,
            pencari_tumpangan=pencari_tumpangan,
            tumpangan=tumpangan
        )
        pengajuan.save()

        notifikasi = NotifikasiPengajuanTumpangan(
            id=uuid.uuid4(),
            user=pencari_tumpangan,
            pengajuan_tumpangan=pengajuan
        )
        notifikasi.save

        return redirect('cari_tumpangan:all_tumpangan')
from django.urls import path
from cari_tumpangan.views import *

app_name = 'cari_tumpangan'

urlpatterns = [
    path('', all_tumpangan, name='all_tumpangan'),
    path('detail/<uuid:tumpangan_id>', tumpangan_detail, name='tumpangan_detail'),
    path('book/', apply_tumpangan, name='apply_tumpangan'),
]
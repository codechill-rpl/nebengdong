from django.apps import AppConfig


class CariTumpanganConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cari_tumpangan'

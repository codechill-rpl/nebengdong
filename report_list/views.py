from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponseNotFound
from nebengdongapp.models import Laporan
from django.contrib.auth.models import User
from django.contrib.admin.views.decorators import staff_member_required
from .forms import UpdateStatusForm
from django.views.decorators.csrf import csrf_exempt

@login_required(login_url='/api/auth/home/')
def show_report_list(request):
    #push
    if request.user.is_staff:
        pengguna = request.user
        data = Laporan.objects.filter(tim_assigned = pengguna)
        if not data.exists():
            message = "Tidak ada riwayat laporan"
            context = {
                'message': message,
                'username': pengguna.username
            }
        else:
            context = {
                'report_data': data,
                'username': pengguna.username
            }
        return render(request, 'report_list.html', context)
    
    return JsonResponse({'failed': 'Not authorized'})

@csrf_exempt
def show_report_list_by_id(request, id):
    if request.user.is_staff:
        report = Laporan.objects.get(id = id)
        if request.method == 'POST':
            form = UpdateStatusForm(request.POST)
            if form.is_valid():
                new_status = form.cleaned_data['new_status']
                report.status = new_status
                report.save()
                return JsonResponse({'success': f'Status of report {id} updated to {new_status}'}, status=200)
        else:
            initial_data = {'new_status': report.status}
            form = UpdateStatusForm(initial=initial_data)

        username = request.user
        context = {
            'report_data': report,
            'username': username,
            'update_status_form': form,
        }
        return render(request, 'detail_report_list.html', context)
    
    return JsonResponse({'failed': 'Not authorized'})
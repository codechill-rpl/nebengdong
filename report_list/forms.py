# forms.py
from django import forms
from nebengdongapp.models.enum_model import StatusLaporan

class UpdateStatusForm(forms.Form):
    new_status = forms.ChoiceField(choices=StatusLaporan.choices, widget=forms.Select(attrs={'class': 'form-control'}))

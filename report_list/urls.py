from django.urls import path
from report_list.views import show_report_list
from report_list.views import show_report_list_by_id

app_name = 'report_list'

urlpatterns = [
    path('', show_report_list, name='show_report_list'),
    path('<uuid:id>', show_report_list_by_id, name='show_report_list_by_id'),
]
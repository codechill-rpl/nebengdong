import os
from django.shortcuts import render
from django.http import JsonResponse
from nebengdongapp.models.tumpangan_model import *

def read_history(request):
    if request.method == 'GET':
        history_pengajuan = PengajuanTumpangan.objects.filter(pencari_tumpangan__user = request.user.id)
        history_tumpangan = Tumpangan.objects.filter(pembuka_tumpangan__user = request.user.id)

        history_pengajuan2 = {}
        for data in history_pengajuan:
            history_pengajuan2[data.id] = data.tumpangan

        flag_pengajuan = 0
        flag_tumpangan = 0

        if (history_pengajuan.exists()):
            flag_pengajuan = 1
        if (history_tumpangan.exists()):
            flag_tumpangan = 1

        context = {
            "base_link" : os.getenv('APP_URL'),
            "flag_pengajuan": flag_pengajuan,
            "flag_tumpangan": flag_tumpangan,
            "history_pengajuan": history_pengajuan2,
            "history_tumpangan" : history_tumpangan
        }

        return render(request, "history_list.html", context)
    
def read_history_detail(request, id_tumpangan):
    if request.method == 'GET':
        detail_history = Tumpangan.objects.get(id = id_tumpangan)

        context={
            "base_link" : os.getenv('APP_URL'),
            "tumpangan" : detail_history
        }
        return render(request, "history_detail_tumpangan.html", context)

def read_history_detail2(request, id_tumpangan):
    if request.method == 'GET':
        detail_history = PengajuanTumpangan.objects.get(id = id_tumpangan)

        context={
            "base_link" : os.getenv('APP_URL'),
            "tumpangan" : detail_history.tumpangan,
            "status_pengajuan": detail_history.status_pengajuan
        }
        return render(request, "history_detail_pengaju.html", context)

from django.urls import path
from .views import *

app_name = 'history'

urlpatterns = [
    path('', read_history, name='read_history'),
    path('tumpangan/<uuid:id_tumpangan>/', read_history_detail, name='read_history_detail'),
    path('pengaju/<uuid:id_tumpangan>/', read_history_detail2, name='read_history_detail2')
]
# 🚌NebengDong
#### Proyek Akhir Mata Kuliah Rekayasa Perangkat Lunak

Link Deployment: [https://nebeng-dong.vercel.app/](https://nebeng-dong.vercel.app/)

Aplikasi "NebengDong" dirancang untuk memudahkan mahasiswa UI dalam mencari dan menyediakan tumpangan sesama mahasiswa. Dengan NebengDong, mahasiswa dapat dengan mudah menemukan rekan-rekan mahasiswa yang memiliki rute perjalanan serupa, sehingga mereka dapat berbagi kendaraan dan biaya perjalanan. Tujuan dari aplikasi ini adalah untuk membantu mengatasi hambatan mobilitas yang dihadapi mahasiswa dan memungkinkan mereka mencapai tujuan mereka di area UI dengan lebih cepat, nyaman, dan ekonomis.

## Kelompok B11 - CodeChill
### Anggota
- 2106701734 - Divany Harryndira
- 2106634332 - Ghayda Rafa Hernawan
- 2106638324 - Naila Shafirni Hidayat
- 2106650222 - Rania Maharani Narendra
- 2106634055 - Syadza Nayla Arrana Desvianto

from django.shortcuts import render
from nebengdongapp.models import *
from nebengdongapp.models.enum_model import *

def index(request):
    if request.user.is_authenticated and request.user.is_superuser == False:
        pengguna = Pengguna.objects.get(user_id=request.user.id)
        no_kendaraan = pengguna.no_kendaraan
        notifs = NotifikasiPengajuanTumpangan.objects.filter(user=pengguna).order_by('-timestamp')[:5]
        tumpangan_list = Tumpangan.objects.filter(status=StatusTumpangan.BUKA).exclude(pembuka_tumpangan=pengguna)[:5]
        pengajuan_tumpangan_dict = {}
        for tumpangan in tumpangan_list:
            pengajuan_tumpangan_dict[tumpangan.id] = PengajuanTumpangan.objects.filter(tumpangan=tumpangan, pencari_tumpangan=pengguna).exists()
    else:
        pengguna = None
        no_kendaraan = ''
        notifs = []
        tumpangan_list = []
        pengajuan_tumpangan_dict = {}
    
    context = {
        'pengguna': pengguna,
        'notifs': notifs,
        'tumpangan_list': tumpangan_list,
        'pengajuan_tumpangan_dict': pengajuan_tumpangan_dict,
        'no_kendaraan': no_kendaraan
    }

    return render(request, 'index.html', context)

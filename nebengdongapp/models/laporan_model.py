from django.db import models
from nebengdongapp.models.user_model import *
from django.contrib.auth.models import User
from nebengdongapp.models.enum_model import StatusLaporan, KategoriLaporan

class Pelapor(models.Model):
    pengguna_pelapor = models.OneToOneField(Pengguna, on_delete=models.SET_NULL, null=True)

class Terlapor(models.Model):
    pengguna_terlapor = models.OneToOneField(Pengguna, on_delete=models.SET_NULL, null=True)

class Laporan(models.Model):
    id = models.UUIDField(primary_key=True)
    tim_assigned = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    pelapor = models.ForeignKey(Pelapor, on_delete=models.SET_NULL, unique=False, null=True)
    terlapor = models.ForeignKey(Terlapor, on_delete=models.SET_NULL, unique=False, null=True)
    timestamp = models.DateTimeField()
    status = models.CharField(max_length=2, choices=StatusLaporan.choices, default=StatusLaporan.DILAPORKAN)
    kategori = models.CharField(max_length=2, choices=KategoriLaporan.choices)
    keterangan = models.TextField()

    def get_status_laporan(self):
        return StatusLaporan(self.status)
    
    def get_kategori_laporan(self):
        return StatusLaporan(self.kategori)
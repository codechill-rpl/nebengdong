from django.db import models
from django.utils.translation import gettext_lazy as _

class StatusTumpangan(models.TextChoices):
        BUKA = 'OP', _('Tumpangan dibuka')
        TUTUP = 'PR', _('Tumpangan ditutup')
        OTW = 'OT', _('Dalam Perjalanan')
        SELESAI = 'SL', _('Selesai')

class StatusPengajuanTumpangan(models.TextChoices):
        MENGAJUKAN = 'Mengajukan'
        DITERIMA = 'Pengajuan Tumpangan Diterima'
        DITOLAK = 'Pengajuan Tumpangan Ditolak'
        SELESAI = 'Selesai'

class StatusLaporan(models.TextChoices):
        DILAPORKAN = 'LP', _('Dilaporkan')
        PROSES = 'PR', _('Diproses Tim Layanan')
        SELESAI = 'SL', _('Laporan Selesai')
        TIDAK_DILANJUTKAN = 'TL', _('Laporan Tidak Dilanjutkan')

class KategoriLaporan(models.TextChoices):
        KECELAKAAN = 'AC', _('Kecelakaan/Darurat')
        BARANG_TEMU = 'FT', _('Menemukan Barang')
        BARANG_HILANG = 'LT', _('Barang Tertinggal/Hilang')
        ISU_DRIVER = 'DI', _('Driver Bermasalah')
        ISU_KENDARAAN = 'VI', _('Kendaraan Bermasalah')
        LAINNYA = 'OT', _('Lainnya')

class LokasiDiUI(models.TextChoices):
        ASRAMA = 'Asrama'
        ST_UI = 'Stasiun UI'
        FPSI = 'Fakultas Psikolosi'
        FISIP = 'Fakultas Ilmu Sosial dan Ilmu Politik'
        FH = 'Fakultas Hukum'
        MUI = 'Masjid Ukhuwah Islamiyah'
        MAC = 'Makara Art Center'
        ST_POCIN = 'Stasiun Pondok Cina'
        RIK = 'Rumpun Ilmu Kesehatan'
        BALAIRUNG = 'Balairung'
        REKTORAT = 'Rektorat'
        PERPUSAT = 'Perpustakaan Pusat'
        PUSGIWA = 'Pusat Kegiatan Mahasiswa'
        FASILKOM_LAMA = 'Fasilkom Gedung Lama'
        FASILKOM_BARU = 'Fasilkom Gedung Baru'
        FKM = 'Fakultas Kesehatan Masyarakat'
        FIK = 'Fakultas Ilmu Keperawatan'
        FMIPA = 'Fakultas MIPA'
        SOR = 'Sarana Olahraga'
        KUTEK = 'Gerbang Kutek'
        KUKEL = 'Gerbang Kukel'
        VOKASI = 'Vokasi'
        FEB = 'Fakultas Ekonomi dan Bisnis'
        FT = 'Fakultas Teknik'
        FIB = 'Fakultas Ilmu Budaya'
        PSJ = 'Pusat Studi Jepang'
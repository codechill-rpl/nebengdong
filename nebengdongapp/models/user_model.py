from django.db import models
from django.contrib.auth.models import User

class TimLayananPengguna(models.Model):
    id = models.UUIDField(primary_key=True)
    nama = models.TextField()
    email = models.TextField()
    password = models.TextField()

class Pengguna(models.Model):
    id = models.UUIDField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=1)
    nama = models.TextField()
    email = models.TextField()
    fakultas = models.TextField()
    no_wa = models.TextField()
    id_line = models.TextField()
    no_kendaraan = models.TextField(null=True)
    is_blocked = models.BooleanField()
    is_on_tumpangan = models.BooleanField()

    def serialize(self):
        return {
                    'id': self.id,
                    'user': self.user.id,
                    'nama': self.nama,
                    'email': self.email,
                    'fakultas': self.fakultas,
                    "no_wa": self.no_wa,
                    "id_line": self.id_line,
                    "no_kendaraan": self.no_kendaraan,
                    "is_blocked": self.is_blocked,
                    "is_on_tumpangan": self.is_on_tumpangan
                }
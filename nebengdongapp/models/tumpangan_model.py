from django.db import models
from nebengdongapp.models.enum_model import LokasiDiUI, StatusTumpangan, StatusPengajuanTumpangan
from nebengdongapp.models.user_model import Pengguna

class Tumpangan(models.Model):
    id = models.UUIDField(primary_key=True)
    lokasi_asal = models.CharField(max_length=50, choices=LokasiDiUI.choices)
    lokasi_tujuan = models.CharField(max_length=50, choices=LokasiDiUI.choices)
    depart = models.DateTimeField()
    kapasitas = models.IntegerField()
    tarif = models.FloatField()
    status = models.CharField(max_length=2, choices=StatusTumpangan.choices)
    deskripsi = models.TextField()
    pembuka_tumpangan = models.ForeignKey(Pengguna, on_delete=models.SET_NULL, null=True)

    def get_lokasi_asal(self):
        return LokasiDiUI(self.lokasi_asal)
    
    def get_lokasi_tujuan(self):
        return LokasiDiUI(self.lokasi_tujuan)
    
    def get_status_tumpangan(self):
        return StatusTumpangan(self.status)

class PengajuanTumpangan(models.Model):
    id = models.UUIDField(primary_key=True)
    status_pengajuan = models.CharField(max_length=50, choices=StatusPengajuanTumpangan.choices)
    pencari_tumpangan = models.ForeignKey(Pengguna, on_delete=models.SET_NULL, null=True)
    tumpangan = models.ForeignKey(Tumpangan, on_delete=models.CASCADE)

    def get_status_pengajuan(self):
        return LokasiDiUI(self.status_pengajuan)
    
    def serialize(self):
        return {
                    'id': self.id,
                    'status_pengajuan': self.status_pengajuan,
                    'pencari_tumpangan': self.pencari_tumpangan.id,
                    'tumpangan': self.tumpangan.id
                }

class NotifikasiPengajuanTumpangan(models.Model):
    id = models.UUIDField(primary_key=True)
    user = models.ForeignKey(Pengguna, on_delete=models.SET_NULL, null=True)
    pengajuan_tumpangan = models.ForeignKey(PengajuanTumpangan, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)

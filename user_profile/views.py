from nebengdongapp.models import Pengguna
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import check_password
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import update_session_auth_hash
from django.shortcuts import render, redirect
from django.core.exceptions import ObjectDoesNotExist

@login_required(login_url='/api/auth/home')
def get_profile_tim(request):
    user = request.user
    try:
        pengguna = Pengguna.objects.get(user_id=user.id)
        return redirect('user_profile:get_profile_pengguna')
    except ObjectDoesNotExist:
        profile_data = {
            'role': "Tim Layanan Pengguna",
            'username': user.username,
            'first_name': user.first_name
        }
        
        return render(request, 'profile_tim.html', profile_data)
    
@login_required(login_url='/api/auth/home')
def get_profile_pengguna(request):
    user = request.user
    try:
        pengguna = Pengguna.objects.get(user_id=user.id)
        status_tumpangan = ""
        
        if pengguna.is_on_tumpangan:
            status_tumpangan = "Aktif"
        else:
            status_tumpangan = "Tidak aktif"

        profile_data = {
            'role': "Pengguna",
            'nama': pengguna.nama,
            'email': pengguna.email,
            'fakultas': pengguna.fakultas,
            'no_wa' : pengguna.no_wa,
            'id_line': pengguna.id_line,
            'no_kendaraan' : pengguna.no_kendaraan,
            'Status_tumpangan': status_tumpangan
        }

        return render(request, 'profile_pengguna.html', {'profile_data': profile_data})
    except ObjectDoesNotExist:
        return redirect('/profile/profile-tim')

@csrf_exempt
@login_required(login_url='/api/auth/home')
def update_password_tim(request):
    try:
        user = request.user
        if request.method == 'POST':
            # Get the current password and new password from the request
            current_password = request.POST.get('current_password')
            new_password = request.POST.get('new_password')
            password_confirm = request.POST.get('confirm_password')
            
            # Check if the new password is empty
            if not new_password:
                return JsonResponse({
                    "status": False,
                    "message": "Password baru tidak boleh kosong.",
                }, status=400)

            # Check if the current password is correct
            if not check_password(current_password, user.password):
                return JsonResponse({
                    "status": False,
                    "message": "Password saat ini salah.",
                }, status=400)
                
            # check if the new password match the password confirmation
            elif new_password != password_confirm:
                print(new_password)
                print(password_confirm)
                return JsonResponse({
                    "status": False,
                    "message": "Password baru yang dimasukkan tidak sama.",
                }, status=400)
        
            # Update the password securely
            user.set_password(new_password)
            user.save()

            # Update the session to prevent the user from being logged out
            update_session_auth_hash(request, user)

            return JsonResponse({
                "status": True,
                "message": "Password baru berhasil disimpan!",
            }, status=200)

        else:
            return JsonResponse({
                "status": False,
                "message": "Invalid request method. Use POST.",
            }, status=405)

    except Exception as e:
        print(e)
        return JsonResponse({
            "status": False,
            "message": "Operation Failed."
        }, status=500)

@csrf_exempt   
@login_required(login_url='/api/auth/home')
def update_profile_pengguna(request):
    # periksa jika rolenya pengguna
    try:
        pengguna = Pengguna.objects.get(user_id=request.user.id)
        if request.method == 'POST':
            new_no_wa = request.POST.get('new_no_wa', '')
            new_id_line = request.POST.get('new_id_line', '')
            new_no_kendaraan = request.POST.get('new_no_kendaraan', '')
            
            # Check if any of the required fields is missing
            if not new_no_wa and not new_id_line:
                return JsonResponse(
                    {
                        "error": "Gagal memperbarui informasi. Mohon beri salah satu kontak."
                    },
                    status=400  # Bad Request
                )
                
            if (new_no_wa and not new_no_wa.isdigit()) or (new_no_wa and int(new_no_wa) <= 0):
                return JsonResponse(
                    {
                        "error": "Gagal memperbarui informasi. No WA yang dimasukkan tidak valid."
                    },
                    status=400  # Bad Request
                )

            # Update if valid
            pengguna.no_wa = new_no_wa
            pengguna.id_line = new_id_line
            pengguna.no_kendaraan = new_no_kendaraan

            pengguna.save()
            return JsonResponse(
                {
                    "message": f"Berhasil memperbarui informasi!"
                }, 
                status=200
            )

    except ObjectDoesNotExist:
        # maka merupakan tim layanan pelanggan
        return redirect('/profile/profile-tim')

from django.urls import path
from .views import *

app_name = 'user_profile'

urlpatterns = [
    path('profile-pengguna/', get_profile_pengguna, name='get_profile_pengguna'),
    path('update-pengguna/', update_profile_pengguna, name='update_profile_pengguna'),
    path('profile-tim/', get_profile_tim, name='get_profile_tim'),
    path('update-password/', update_password_tim, name='update_password_tim'),
]
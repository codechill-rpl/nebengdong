# Install dependencies from requirements.txt
pip install -r requirements.txt

# Apply database migrations
python3 manage.py makemigrations
python3 manage.py migrate

# Collect static files
python3 manage.py collectstatic --noinput